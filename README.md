# Test

## Using Memaid flowcharts

```mermaid
graph TD;
        A("💻Emoji usage in Mermaid")-->B("Multi line support in Mermaid <br/>     Centering text is manual")-->C("Formatting text in <br/>Mermaid diagrams")-->D("Designing in Mermaid")
        C-->J("Centering text in Mermaid")-->K("Unicode whitespace characters only")
        J-->HEHE("Centering using CSS<br/> here we go")
        J-->U("No other way, HTML markup do not work in Mermaid")
        C-->H("Highlighting seperate words in Mermaid")
        H--> F("𝐁𝐨𝐥𝐝 𝐭𝐞𝐱𝐭 𝐠𝐞𝐧𝐞𝐫𝐚𝐭𝐨𝐫")
        H--> G("HTML Markup Bold and Italics do not work in Marmaid")
        click A "https://github.com/mermaid-js/mermaid/issues/625#issuecomment-558295512" "This is a link"
        click B "https://github.com/mermaid-js/mermaid/issues/384#issuecomment-373703081" "This is a link"
        click D "https://github.com/mermaid-js/mermaid/issues/924#issuecomment-527772750" "This is a link"
classDef test fill:lightblue,color:green,stroke:#FF9E2C,font-weight:bold
class D test
 style HEHE text-align: center, border: solid 1px black, fill:lightblue, line-height: 90px, position: relative, display: flex, justify-content: center, align-items: center;
        
```



### Next testings


```mermaid
graph TD;
        A-->B("hello 💻")
        click A callback "Tooltip"
        click B "http://www.github.com" "This is a link"
```

<b>test</b>
<pre><code>`mermaid
graph TD;
  A-->B;
  A-->C;
  B-->D;
  C-->D;
</code</pre>>